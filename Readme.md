To run this 


Install everthing in pipfile

run `docker-compose up`

Go to `front` folder and run `npm install` and run `npm run serve`

Go back to django main project and run `python manage.py runserver` 

Debugging Purpose 
run `celery flower -A Project4 --address=127.0.0.1 --port=5555`  for celery monitoring (can be ignored) 

run `celery -A Project4 worker -O fair`   

With `-O fair` this option enabled the worker will only write to processes that are available for work, disabling the prefetch behavior:

`https://docs.celeryproject.org/en/stable/userguide/optimizing.html#prefork-pool-prefetch-settings`


start by uploading the file at `http://localhost:8080/`

