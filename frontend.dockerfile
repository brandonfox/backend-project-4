# build stage
FROM node:lts-alpine as build-stage
WORKDIR /app
ARG VUE_APP_BACKEND_URL
ARG VUE_APP_BACKEND_PORT
ARG VUE_APP_MINIO_ENDPOINT
ENV VUE_APP_BACKEND_URL=$VUE_APP_BACKEND_URL
ENV VUE_APP_BACKEND_PORT=$VUE_APP_BACKEND_PORT
ENV VUE_APP_MINIO_ENDPOINT=$VUE_APP_MINIO_ENDPOINT
COPY ./front/package*.json ./
RUN npm install
COPY ./front .
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]