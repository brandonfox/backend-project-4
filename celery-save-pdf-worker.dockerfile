FROM python:3.7

RUN useradd -u 1000 c

RUN apt-get update

RUN apt-get -y install build-essential libpoppler-cpp-dev pkg-config python3.7-dev

WORKDIR /home/c

COPY Pipfile .

RUN pip install pipenv

RUN chown c:c -R /home/c/

USER c

RUN pipenv install

COPY ./Project4 ./Project4

COPY ./zipAndText ./zipAndText

COPY ./progress ./progress

COPY manage.py .

ENTRYPOINT [ "pipenv", "run","celery","-A","Project4","worker","-Q","save_pdf_queue","-l","info"]