from django.http import HttpResponse, JsonResponse
from Project4.file_storage import FileStorage
from Project4.celery import app
from celery import chord
from .tasks import save_pdf, parse_pdf, tar_task
from .utility import get_hash
from Project4.datastore import *


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def success(request):
    return HttpResponse("Success")


def check_status(request, status_id):
    result = app.AsyncResult(status_id)
    if result.ready():
        response = {'ready': True, 'file_url': result.result}
    else:
        response = {'ready': False, 'file_url': ''}
    return JsonResponse(response)


def upload_file(request, file_name):
    import tarfile
    mediaFile = FileStorage()
    hsh = get_hash(file_name)
    tr = tarfile.open(fileobj=mediaFile.open(file_name))
    names = tr.getnames()
    guard = [name for name in names if name.startswith('.') is False]
    create_task(hsh)
    complete_unzip(hsh, len(names))
    c = [save_pdf.s(file_name, name, hsh) | parse_pdf.s(hsh) for name in guard]
    tasks_chain = chord(c)(tar_task.si(guard, file_name, hsh))
    # this is the id of the whole task we can use to see if its done
    result = {'groupId': hsh}
    # return the group task id
    return JsonResponse(result)
