import tarfile
import os
from celery import shared_task, group, chord
from celery.utils.log import get_task_logger
import pdftotext
from tempfile import NamedTemporaryFile
from Project4.file_storage import FileStorage
from Project4.datastore import *
from .utility import get_hash

logger = get_task_logger(__name__)

mediaFile = FileStorage()


@shared_task(bind=True, ignore_result=False)
def tar_task(self, ls, zip_name, task_id):
    tar_name = "{}_converted_{}".format(get_hash(zip_name), zip_name)
    with tarfile.open(tar_name, "w:gz") as tar:
        for name in ls:
            text_file_name = name.split('.')[0] + '.txt'
            logger.info("Reading " + text_file_name)
            size = mediaFile.size(text_file_name)
            with mediaFile.open(text_file_name) as source:
                info = tar.gettarinfo(fileobj=source)
                tar.addfile(info, fileobj=source)
                mediaFile.delete(text_file_name)

    with open(tar_name, "rb") as f:
        mediaFile.save(tar_name, f)
    os.remove(tar_name)
    complete_tar(task_id)
    return mediaFile.url(tar_name)


@shared_task(bind=True)
def parse_pdf(self, pdf_file, task_id):
    logger.info("Parsing pdf for file: {}, task: {}".format(pdf_file, task_id))
    # Load your PDF
    with mediaFile.open(pdf_file) as f:
        pdf = pdftotext.PDF(f)

    text_file_name = pdf_file.split('.')[0] + '.txt'

    open_to_write = NamedTemporaryFile('w', delete=False)
    logger.info(open_to_write.name)
    with open_to_write as f:
        logger.info(f)
        f.write("\n\n".join(pdf))
    with open(open_to_write.name, 'rb') as f:
        mediaFile.save(text_file_name, f)
    mediaFile.delete(pdf_file)
    complete_pdf(task_id)
    return text_file_name


@shared_task(bind=True)
def save_pdf(self, tar_name, pdf_name, task_id):
    tr = tarfile.open(fileobj=mediaFile.open(tar_name))
    mediaFile.save(pdf_name, tr.extractfile(pdf_name))
    tr.close()
    return pdf_name
