from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path("success", views.success, name="success"),
    path("instantiate/<str:file_name>", views.upload_file, name="upload"),
    path("statusOf/<str:status_id>", views.check_status, name="check_status")

]
