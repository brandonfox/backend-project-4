import Vue from 'vue'
import App from './App.vue'
import VueNativeSock from 'vue-native-websocket'

export function getEnvironmentVar(key,defaultVal){
  var result = process.env[`VUE_APP_${key}`];
  window.console.log(`Trying to read environment variable: ${key}, got: ${result}`)
  if(result!= undefined)
    return result
  else 
    return defaultVal
}

export const BACKEND_URL = getEnvironmentVar("BACKEND_URL","localhost")
export const BACKEND_PORT = getEnvironmentVar("BACKEND_PORT",8000)

Vue.use(VueNativeSock, `ws://${BACKEND_URL}:${BACKEND_PORT}/ws/progress/`, {
  connectManually: true,
})

const Minio = require('minio')

export const client = new Minio.Client({
    endPoint: getEnvironmentVar("MINIO_ENDPOINT","127.0.0.1"),
    port: getEnvironmentVar("MINIO_PORT",9000),
    useSSL: false,
    accessKey: getEnvironmentVar("MINIO_ACCESS_KEY",'minioadmin'),
    secretKey: getEnvironmentVar("MINIO_SECRET_KEY",'minioadmin')
})


client.bucketExists("uploads", (err, exists) => {
  if (err) {
      return console.log(err)
    }
    if (exists) {
      return console.log('Bucket exists.')
    } else {
      client.makeBucket('uploads', function(err) {
          if (err) return console.log('Error creating bucket.', err)
          console.log('Bucket created successfully')
        })
    }
})
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
