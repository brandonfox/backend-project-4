from Project4.datastore import *

def datastore_test():
    NO_TEST_PDFS = 24
    FIRST_TEST = 14
    task_id = "123"
    create_task(task_id)
    print("Created task with id: " + task_id + ", percentage: " + str(get_task_as_percentage(task_id)))
    complete_unzip(task_id,NO_TEST_PDFS)
    print("Completed unzip: 20 pdfs, percentage: " + str(get_task_as_percentage(task_id)))
    for i in range(FIRST_TEST):
        complete_pdf(task_id)
    print("Completed " + str(FIRST_TEST) + " pdfs, percentage: " + str(get_task_as_percentage(task_id)))
    for i in range(NO_TEST_PDFS - FIRST_TEST):
        complete_pdf(task_id)
    print("Completed " + str(NO_TEST_PDFS) + " pdfs, percentage: " + str(get_task_as_percentage(task_id)))