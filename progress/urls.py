from django.urls import path

from . import views

urlpatterns = [
    path('<str:task_id>', views.view_task, name='view_task'),
]