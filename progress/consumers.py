from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from Project4.datastore import get_task_as_percentage
import json

class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.id = self.scope['url_route']['kwargs']['task_id']
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.id,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        if message == 'status':
            self.send(text_data=json.dumps({
                'progress': get_task_as_percentage(self.id)
            }))
    
    # Receive message from room group
    def update(self, event):
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'progress': get_task_as_percentage(self.id)
        }))