from  django.core.cache  import  cache 
from Project4.progress_broadcaster import send_update

DEFAULT_TIMEOUT = 60*60*5 # 5 hours

def create_task(task_id):
    cache.set(task_id,0,timeout=DEFAULT_TIMEOUT) #5 hour expiry, 0 for not yet completed upload, 1 for completed
    cache.delete(task_id + "-total")
    cache.delete(task_id + "-done")

def get_task_timeout(task_id):
    return cache.ttl(task_id)

def complete_unzip(task_id, no_pdfs):
    timeout = get_task_timeout(task_id) + DEFAULT_TIMEOUT
    cache.set(task_id + "-total",no_pdfs,timeout=timeout)
    cache.set(task_id + "-done",0,timeout=timeout)
    send_update(task_id)

def complete_pdf(task_id): #Returns files remaining
    timeout = get_task_timeout(task_id) + DEFAULT_TIMEOUT
    files_total = get_files_total(task_id)
    if(files_total == None):
        raise Exception("Total files for " + task_id + " has not yet been set")
    files_done = get_files_done(task_id)
    files_done_key = task_id + "-done"
    if(files_done == None):
        cache.set(files_done_key,1,timeout=timeout)
        send_update(task_id)
        return files_total - 1
    else:
        total = files_total - cache.incr(files_done_key)
        send_update(task_id)
        return total

def complete_tar(task_id):
    cache.set(task_id,1,timeout=DEFAULT_TIMEOUT) # if set to 1 then processing has completed
    send_update(task_id)

def get_task_completion(task_id):
    return cache.get(task_id)

def get_files_done(task_id):
    return cache.get(task_id + "-done")

def get_files_total(task_id):
    return cache.get(task_id + "-total")

def get_task_as_percentage(task_id):
    task = get_task_completion(task_id)
    if(task == None): #Task does not exist
        return 0
    if(task == 1):
        return 100
    files_done = get_files_done(task_id)
    files_total = get_files_total(task_id)
    if(files_total == None): #Unzip has not yet finished
        return 0
    elif(files_done == None):
        return 1
    else:
        return int(files_done/float(files_total) * 98) + 1