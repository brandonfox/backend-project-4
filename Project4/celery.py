from __future__ import absolute_import, unicode_literals

import os

from celery import Celery


# set the default Django settings module for the 'celery' program.
from kombu import Queue

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Project4.settings')

app = Celery('Project4')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.conf.task_queues = [
    Queue("pdf_parser_queue"),
    Queue("save_pdf_queue"),
    Queue("tar_queue"),
]
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
