from asgiref.sync import async_to_sync
from channels.layers import *

def send_update(task_id):
    print("Sending update")
    layer = get_channel_layer()
    async_to_sync(layer.group_send)(task_id,{
        "type": "update",
    })